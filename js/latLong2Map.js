function initialize() {
        var center = new google.maps.LatLng(37.4419, -122.1419);
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 4,
          center: center,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        var markers = [];
        for (var i = 0; i < data.length; i++) {
          var dataLoc = data[i];
            if (dataLoc.count > 1) {
                for (var j = 0; j < dataLoc.count; j++) {
                    var latLng = new google.maps.LatLng(dataLoc.latitude, dataLoc.longitude);
                    var marker = new google.maps.Marker({ position: latLng });
                }
            }  else {
                  var latLng = new google.maps.LatLng(dataLoc.latitude, dataLoc.longitude);
                  var marker = new google.maps.Marker({ position: latLng });
            }
          markers.push(marker);
        }
        var markerCluster = new MarkerClusterer(map, markers);
      }
      google.maps.event.addDomListener(window, 'load', initialize);